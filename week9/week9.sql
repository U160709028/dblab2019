create view USA_customers as
select CustomerID, CustomerName, ContactName
from Customers
where Country = "USA";

select * from USA_customers;

select *
from USA_customers join Orders on USA_customers.CustomerID = Orders.CustomerID;

select avg(price) from Products;

create or replace view Products_below_avg_price as
select ProductID, ProductName, Price
from Products
where Price < 30;

select *
from USA_customers join Orders on USA_customers.CustomerID = Orders.CustomerID;
where OrderID in (
	select OrderID
	from OrderDetails join Products_below_avg_price on OrderDetails.ProductID = Products_below_avg_price.ProductID
);

DROP view USA_customers;
