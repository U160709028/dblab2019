-- -
-- Update
-- -
select * from Customers;

select CustomerName
from Customers
where Country like "Mexi%";

update Customers
set CustomerName = "Juan"
where Country like "Mexi%";
-- -
-- Insert
-- -
select * from Employees;

Insert Into Employees (EmployeeID, LastName, FirstName, BirthDate, Photo, Notes)
Values (11, "Yaz", "Sander", "1997-23-07", "" , "Sander");
-- -
-- Delete
-- -
select * from Customers;
Delete from Customers where CustomerID = 20;

select * from Orders where CustomerID = 20;
delete from Orders where CustomerID = 20;

select * from OrderDetails;
select * from OrderDetails where OrderID in (
	select OrderID from Orders where CustomerID = 20
);

delete from OrderDetails where OrderID in (
	select OrderID from Orders where CustomerID = 20
);
delete from Orders where CustomerID = 20;
Delete from Customers where CustomerID = 20;
-- -
select * from Products;
delete from Products where ProductID = 10;

select * from OrderDetails where ProductID = 10;
delete from OrderDetails where ProductID = 10;
-- -
select * from Shippers;
select * from Orders where ShipperID = 2;

delete from Orders where ShipperID = 2;
delete from Shippers where ShipperID = 2;

select * from OrderDetails where OrderID in (
	select OrderID from Orders where ShipperID = 2
);
delete from OrderDetails where OrderID in (
	select OrderID from Orders where ShipperID = 2
);
delete from Orders where ShipperID = 2;
delete from Shippers where ShipperID = 2;



